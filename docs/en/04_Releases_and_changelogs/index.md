title: Releases and changelogs
summary: Upgrading guides, changelogs and support timeframes for each released version of the CWP recipe codebase.
introduction: The status of the recipe releases is summarised in the table below. Upgrading information for each version is available in the changelog.

# Releases and changelogs

| Recipe version | Description | Release date | Support ends date |
| -------------- | ----------- | ------------ | ----------------- |
| [1.3.0](cwp_recipe_basic_1.3.0) | Tracks the Framework release 3.3.1. | 29/02/2016 | current |
| [1.2.0](cwp_recipe_basic_1.2.0) | Tracks the Framework release 3.2.1 and includes minor security fixes. | 9/12/2015 | 29/08/2017 |
| [1.1.2](cwp_recipe_basic_1.1.2) | Tracks the Framework release 3.1.15 and includes minor security fixes. | 21/09/2015 | 1/03/2016 |
| [1.1.1](cwp_recipe_basic_1.1.1) | Introduces many new features to UserForms, Solr, Full-text search and QueuedJobs modules. | 1/09/2015 | 1/03/2016 |
| [1.1.0](cwp_recipe_basic_1.1.0) | Tracks the Framework release 3.1.13, including bugfixes and major features | 05/06/2015 | 1/03/2016 |
| [1.0.7](cwp_recipe_basic_1.0.7) | Tracks the Framework release 3.1.12, including security and bug fixes | 26/03/2015 | 1/03/2016 |
| [1.0.6](cwp_recipe_basic_1.0.6) | Tracks the Framework release 3.1.10, including security and bug fixes | 26/02/2015 | 31/05/2015 |
| [1.0.5](cwp_recipe_basic_1.0.5) | Tracks the Framework release 3.1.8, including bug fixes and features. | 24/11/2014 | 31/05/2015 |
| [1.0.4](cwp_recipe_basic_1.0.4) | Tracks the Framework release 3.1.6, including bug fixes and features. | 26/08/2014 | 31/05/2015 |
| [1.0.3](cwp_recipe_basic_1.0.3) | Tracks the Framework release 3.1.5, including security and bug fixes. | 31/05/2014 | 31/05/2015 |
| [1.0.2](cwp_recipe_basic_1.0.2) | Tracks the Framework release 3.1.2 and fixes some module bugs. | 30/01/2014 | 31/05/2015 |
| [1.0.1](cwp_recipe_basic_1.0.1) | First release of the recipe and all related modules. | 12/11/2013 | 31/05/2015 |

# Other supported module releases

Currently all supported modules are a part of the basic recipe.
